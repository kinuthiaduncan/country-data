$(function() {
    $(".owl-carousel").owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        items: 1,
        dots: true,
        autoplay: true,
        autoplaySpeed: 1000,
        autoplayTimeout: 6000
    });
});
