<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    use HasFactory;
    protected $table='Variables';
    protected $guarded=['VariableID'];
    protected $fillable=[
        'VariableID',
        'VariableName',
        'SubcategoryID',
        'VariableDescription',
        'xAxisLabel',
        'yAxisLabel',
        'AdditionalText',
        'SSMA_TimeStamp'
    ];

    public function subCategory() {
        return $this->belongsTo('Models\SubCategory', 'SubcategoryID');
    }
    public function scopeExclude($query, $value = ['SSMA_TimeStamp'])
    {
        return $query->select(array_diff($this->fillable, (array) $value));
    }
}
