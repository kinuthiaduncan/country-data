<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    protected $table='Countries';
    protected $fillable=[
        'CountryID',
        'CountryName',
        'Population',
        'PopulationYear',
        'AreaSqKm',
        'GDPPerCapita',
        'GDPPerCapitaYear',
        'GDPBillionCurrent',
        'GDBBillionCurrentYear',
        'GINIIndex',
        'GINIIndexYear',
        'EaseOfBusinessRank',
        'SocialProgressIndexScore',
        'SocialProgressIndexRankYear',
        'CountryFlagImageFile',
        'Predident',
        'DeputyPresident',
        'CapitalCity',
        'Languages',
        'SSMA_TimeStamp'
    ];
    public function scopeExclude($query, $value = ['SSMA_TimeStamp'])
    {
        return $query->select(array_diff($this->fillable, (array) $value));
    }
}
