<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;
    protected $table='Subcategories';
    protected $fillable=[
        'SubcategoryID',
        'CategoryID',
        'SubCategoryName',
        'SSMA_TimeStamp'
    ];

    public function category() {
        return $this->belongsTo('Models\Category');
    }
    public function variables() {
        return $this->hasMany(Variable::class, 'SubcategoryID', 'SubcategoryID');
    }
    public function scopeExclude($query, $value = ['SSMA_TimeStamp'])
    {
        return $query->select(array_diff($this->fillable, (array) $value));
    }
}
