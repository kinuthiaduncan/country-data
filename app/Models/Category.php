<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table='Categories';
    protected $fillable=[
        'CategoryID',
        'CategoryName',
        'SSMA_TimeStamp'
    ];
    public function subCategories() {
        return $this->hasMany(SubCategory::class, 'CategoryID', 'CategoryID');
    }
    public function scopeExclude($query, $value = ['SSMA_TimeStamp'])
    {
        return $query->select(array_diff($this->fillable, (array) $value));
    }
}
