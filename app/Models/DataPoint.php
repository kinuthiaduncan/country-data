<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPoint extends Model
{
    use HasFactory;
    protected $table='DataPoints';
    protected $fillable=[
        'DataPointID',
        'VariableID',
        'DataPointValue',
        'Year',
        'CountryID',
        'SSMA_TimeStamp'
    ];

    public function variable() {
        return $this->belongsTo('App\Models\Variable', 'VariableID', 'VariableID');
    }
    public function country() {
        return $this->belongsTo('App\Models\Country', 'CountryID', 'CountryID');
    }
    public function scopeExclude($query, $value = ['SSMA_TimeStamp'])
    {
        return $query->select(array_diff($this->fillable, (array) $value));
    }
}
