<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Country;
use App\Models\DataPoint;
use App\Models\SubCategory;
use App\Models\Variable;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::select('CountryName', 'CountryID')->get();
        return response([
            'success' => true,
            'data' => $countries
        ], 200);
    }

    public function countries()
    {
        $countries = Country::select('CountryName', 'CountryID')->get();
        return view('countries')->with('countries',  $countries);
    }

    /**
     * Display the specified resource.
     *
     * @param $countryId
     * @return \Illuminate\Http\Response
     */
    public function getCountry($countryId): \Illuminate\Http\Response
    {
        $countryData = Country::where('CountryID', number_format($countryId))->exclude()->get();
        return response([
            'success' => true,
            'data' => $countryData
        ], 200);
    }
    public function fetchCategories() {
        $categories = Category::exclude()->get();
        return response([
            'success' => true,
            'data' => $categories
        ], 200);
    }
    public function fetchSubCategories($categoryId) {
        $order = 'asc';
        $subCategories = SubCategory::where('CategoryID', number_format($categoryId))->exclude()
            ->with(['variables' => function ($q) use ($order) {
                $q->orderBy('VariableName', $order)->exclude();
            }])->get();
        return response([
            'success' => true,
            'data' => $subCategories
        ], 200);
    }

    public function country($countryName) {
        $countryData = Country::where('CountryName', $countryName)->exclude()->first();
        return view('data')->with('country',  $countryData)->with('auth_user',  auth()->user());
    }

    public function fetchVariableData($variableId, $countryId) {
        $variable = Variable::where('VariableID', number_format($variableId))->exclude()->first();
        $dataPoint = DataPoint::select('VariableID', 'DataPointValue', 'Year', 'CountryID')
            ->where('CountryID', number_format($countryId))
            ->where('VariableID', number_format($variableId))
            -> orderBy('Year', 'desc')
            ->limit(10)
            ->get();
        return response([
            'success' => true,
            'data' => $dataPoint,
            'variable' => $variable
        ], 200);
    }

    public function fetchAllVariableData($variableId, $countryId) {
        $dataPoint = DataPoint::select('VariableID', 'DataPointValue', 'Year', 'CountryID')
            ->where('CountryID', number_format($countryId))
            ->where('VariableID', number_format($variableId))
            -> orderBy('Year')
            ->get();
        return response([
            'success' => true,
            'data' => $dataPoint,
        ], 200);
    }
}
