@extends('welcome')

@section('content')
    <div class="hero">
        <div class="owl-carousel">
            <div class="item" style="background-image:url(https://res.cloudinary.com/dc4rgage6/image/upload/v1616049032/hero1_tguqyq.jpg)">
                <div class="container text-center">
                    <h2 class="text-white">
                        Welcome to ArcCentre
                    </h2>
                </div>
            </div>
            <div class="item" style="background-image:url(https://res.cloudinary.com/dc4rgage6/image/upload/v1616049032/hero2_fkds9l.jpg)">
                <div class="container text-center">
                    <h2 class="text-white">

                        ArCentre: experts in research, consulting, and data management in Africa
                    </h2>
                </div>
            </div>
            <div class="item" style="background-image:url(https://res.cloudinary.com/dc4rgage6/image/upload/v1618324132/flags_qcuduc.jpg)">
                <div class="container text-center">
                    <h2 class="text-white">
                        ArcCentre: experts in research, consulting, and data management in Africa
                    </h2>
                </div>
            </div>
            <div class="item" style="background-image:url(https://res.cloudinary.com/dc4rgage6/image/upload/v1616049032/hero3_rtvqnu.jpg)">
                <div class="container text-center">
                    <h2 class="text-white">
                        ArcCentre: helping to unlock Africa's growth potential
                    </h2>
                </div>
            </div>

        </div>

    </div>
    <section class="py-5 my-2">
        <h2 class="text-center mb-5">Our Services</h2>
        <div class="container-xxl">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <img src="https://res.cloudinary.com/dc4rgage6/image/upload/v1616049031/research_h8o20h.jpg" class="img-fulid" alt="">
                    <h3 class="text-center my-3">
                        Research
                    </h3>
                    <p class="text-center fw-semibold body-text">At ArcCentre, we pride ourselves as leaders in economic and business research in Africa. We are working with highly skilled and
                        experienced
                        researchers
                        whose insightful work
                        have aided policy and decision making. The originality and quality of our research have brought new perspectives in our practice areas. The researchers at ArcCentre are the
                        mainstay of the firm and the underlying reason why we are able to offer supreme, unparalleled services to our clients.</p>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="https://res.cloudinary.com/dc4rgage6/image/upload/v1616049032/conuslting_q302ig.jpg" class="img-fulid" alt="">
                    <h3 class="text-center my-3">
                        Consulting
                    </h3>
                    <p class="text-center fw-semibold body-text">ArcCentre provides a wide range of consulting services in areas of economics, management, business, climate, and energy. Our team
                        of consultants are drawn from world-class professionals who have
                        deeper knowledge and understanding of the complexities of the African continent, its people, culture, and potential. They also bring to the fore their knowledge and
                        experience from different
                        industries and sectors.</p>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="https://res.cloudinary.com/dc4rgage6/image/upload/v1616049033/data_st8fs2.png" class="img-fulid" alt="">
                    <h3 class="text-center my-3">
                        Data
                    </h3>
                    <p class="text-center fw-semibold body-text">One of ArcCentre’s unique products is the data it provides on its database system. Meaningful research relies on trusted data. In
                        recognizing this fact, we have created a database system to make data
                        provision a fundamental aspect of our core business. The data include national statistics of various African countries and covers wide range of sectors. The ArcCentre
                        database has enabled our clients
                        access comprehensive data that help them to make informed decisions. The system is administered by our team of trusted sources, and the data helps capture emerging
                        economic, financial, political,
                        social, and industry-specific events.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5  bg-light">
        <div class="container-xxl text-center">
            <h2 class="text-center mb-5">Our Clients</h2>
            <div class="row">
                <div class="col-md">
                    <p class="fw-bold">Government and public sector</p>
                </div>
                <div class="col-md">
                    <p class="fw-bold">Private sector</p>
                </div>
                <div class="col-md">
                    <p class="fw-bold">NGOs</p>
                </div>
                <div class="col-md">
                    <p class="fw-bold">Multinational organisations</p>
                </div>
                <div class="col-md">
                    <p class="fw-bold">International development agencies</p>
                </div>
            </div>
        </div>
    </section>
@endsection
