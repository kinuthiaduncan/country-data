@extends('welcome')

@section('content')
    <section class="py-5 my-2">

        <div class="container-xxl">
            <div class="row align-items-center mb-5">

                <div class="col-lg-5">
                    <img src="https://res.cloudinary.com/dc4rgage6/image/upload/v1616049031/research_h8o20h.jpg" class="w-100" alt="">
                </div>
                <div class="col-lg-7 pl-4">
                    <h2 class=" mb-3">Research</h2>

                    <p class="fw-semibold ">At ArcCentre, we pride ourselves as leaders in economic and business research in Africa. We are working with highly skilled and experienced researchers
                        whose insightful work have aided policy and
                        decision making. The originality and quality of our research have brought new perspectives in our practice areas. The researchers at ArcCentre are the mainstay of the firm
                        and the underlying reason
                        why we are able to offer supreme, unparalleled services to our clients.
                    </p>
                </div>
            </div>
            <div class="row align-items-center  mb-5">

                <div class="col-lg-5">
                    <img src="https://res.cloudinary.com/dc4rgage6/image/upload/v1616049032/conuslting_q302ig.jpg" class="w-100" alt="">
                </div>
                <div class="col-lg-7 pl-4">
                    <h2 class=" mb-3">Consulting</h2>

                    <p class="fw-semibold ">ArcCentre provides a wide range of consulting services in areas of economics, management, business, climate, and energy. Our team of consultants are
                        drawn from world-class professionals who have
                        deeper knowledge and understanding of the complexities of the African continent, its people, culture, and potential. They also bring to the fore their knowledge and
                        experience from different
                        industries and sectors.
                    </p>
                </div>
            </div>
            <div class="row align-items-center  mb-5">

                <div class="col-lg-5">
                    <img src="https://res.cloudinary.com/dc4rgage6/image/upload/v1616049033/data_st8fs2.png" class="w-100" alt="">
                </div>
                <div class="col-lg-7 pl-4">
                    <h2 class=" mb-3">Data</h2>

                    <p class="fw-semibold ">One of ArcCentre’s unique products is the data it provides on its database system. Meaningful research relies on trusted data. In recognizing this fact,
                        we have created a database system to make data
                        provision a fundamental aspect of our core business. The data include national statistics of various African countries and covers wide range of sectors. The ArcCentre
                        database has enabled our clients
                        access comprehensive data that help them to make informed decisions. The system is administered by our team of trusted sources, and the data helps capture emerging
                        economic, financial, political,
                        social, and industry-specific events.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
