@extends('welcome')

@section('content')
    <section class="py-3">
        <div class="container-xxl">
            <div class="row" id="app">
                <div class="col-lg-2 col-md-4 data-navbar-vertical">
                    <sidebar :auth_user="'{{ $auth_user }}'" :country="'{{ $country }}'"></sidebar>
                </div>
                <div class="col-lg-10 col-md-8">
                    <router-view></router-view>
                </div>
            </div>
        </div>
    </section>
@endsection
