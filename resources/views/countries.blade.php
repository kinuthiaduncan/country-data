@extends('welcome')

@section('content')
    <section class="py-5 my-2">

        <div class="container-xxl">
            <h3>Data Atlas</h3>
            <hr>
            <div class="row align-items-center  mb-5">
                @if($countries->count() > 0)
                    @foreach($countries as $country)
                        <div class="col-6">
                            <a href="/country/{{$country->CountryName}}" class="h5 mb-4 d-inline-block text-decoration-none">{!! $country->CountryName !!}</a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection
