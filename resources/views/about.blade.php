@extends('welcome')

@section('content')
    <section class="py-5 my-2">

        <div class="container-xxl">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class=" mb-3">About us</h2>

                    <p class="fw-semibold ">ArcCentre, legally known as Africa Research and Consulting Centre, is an international consulting and data management firm
                        specialised in providing wide range of consultancy services in areas of
                        economics, business, and management in Africa and beyond. We aim to advice governments and organizations based in Africa, as well as international organizations, non-profit
                        organizations, and
                        multinational companies that plan to approach the African market to advance their impact in Africa. We are committed to providing high-quality services that produce maximum
                        benefits to our clients.
                        <br /><br />
                        Our team include experts drawn from world-class professionals who have deeper knowledge and understanding of the African continent, its people, culture, and potential, but
                        who also have a global world
                        view of knowledge and experience across different industries and sectors.
                        <br /><br />
                        One of our core business areas also include data management which is aided by a comprehensive database system. This database enables our clients access reliable
                        decision-making data on Africa. The
                        system is administered by our team from trusted sources and it helps capture emerging economic, financial, political, social, and industry-specific trends. This sets us
                        apart from our competitors.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
