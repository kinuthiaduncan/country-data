@extends('welcome')

@section('content')
    <section class="py-5 my-2">

        <div class="container-xxl">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-6">
                     <h2 class="text-center mb-3">Login</h2>
                    <div class="card shadow">
                        <div class="card-body p-4">



                    <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="mb-3">
                        <label for="email" class="form-label">{{ __('Name') }}</label>
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">{{ __('Password') }}</label>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary w-100 px-4">
                        {{ __('Login') }}
                    </button>

                            {{--                                @if (Route::has('password.request'))--}}
                            {{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
                            {{--                                        {{ __('Forgot Your Password?') }}--}}
                            {{--                                    </a>--}}
                            {{--                                @endif--}}
                </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
