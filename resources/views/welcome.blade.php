<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no; maximum-scale=1.0; user-scalable=no;" />
        <link rel="icon" type="image/png" href="/images/favicon.png"/>
        <title>ArcCenter</title>
        <link rel="stylesheet" href="{{mix('/css/app.css')}}"/>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
    <div id="main">
        <nav class="navbar navbar-expand-lg navbar-light bg-white menu">
            <div class="container-xxl">
                <a class="navbar-brand" href="/"><img src="/images/logo.png" width="250px" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse  justify-content-end" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link active" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">About us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/country-list">Data Center</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link">Publications (to come)</a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="/services">Services</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="/practice">Practice Areas</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="/contact">Contact us</a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </div>
        </nav>
        <div class="">
            @yield('content')
        </div>
        <footer style="background-image: url(/images/footer-dark.jpg);">
            <div class="container-xxl body-text">
               <div class="container-xxlx body-text">
                <div class="row justify-content-end m-0">

                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h5 class="text-white">Useful Links</h5>
                                <ul class="nav d-flex flex-column">
                                    <li><a href="/about">About us</a></li>
                                    <li><a href="/practice">Practice Areas</a></li>
                                    <li><a href="">Mission & Vision</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5 class="text-white">Quick Links</h5>
                                <ul class="nav d-flex flex-column">
                                    <li><a href="/country-list">Data Centre</a></li>
                                    <li><a href="/services">Services</a></li>
                                    <li><a href="/contact">Contact Us</a></li>
                                </ul>
                                 <ul class="nav flex-row">
                                    <li><a class="d-block pe-2" rel="nofollow" href="#" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
                                    <li><a class="d-block px-2" rel="nofollow" href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                    <li><a class="d-block px-2" rel="nofollow" href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-xxl mt-5 body-text">
                <div class="row align-items-center">
                    <div class="col-sm">
                        <ul class="nav d-flex flex-row">
                            <li><a href="">Terms of Use</a></li>
                            <li class="px-2 py-1">/</li>
                            <li><a href="">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-auto">
                        <p class="m-0 fw-semibold">© 2020 All Rights Reserved.</p>
                    </div>
                </div>

            </div>
        </footer>
    </div>
    <script>
        window.Laravel = {csrfToken: '{{ csrf_token() }}'};
    </script>
    <script src="{{mix('/js/app.js')}}"></script>
    <script src="/js/jqueryScripts.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="https://kit.fontawesome.com/fa99abf141.js" crossorigin="anonymous"></script>
    </body>
</html>
