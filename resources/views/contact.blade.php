@extends('welcome')

@section('content')
    <section class="py-5 my-2">

        <div class="container-xxl">
            <div class="row ">

                <div class="col-lg-6">
                    <h2 class=" mb-3">Contact Infomation</h2>
                    <p class="fw-semibold "><strong>Email: </strong>services@arccentre.net</p>
                </div>
                <div class="col-lg-6">
                    <h2 class=" mb-3">Leave us message</h2>

                    <form name="form" id="form">
                        <div class="mb-3">
                            <label for="name" class="form-label">Your Name (required) </label>
                            <input type="text" class="form-control" name="name" id="name">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Your Email (required) </label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Your Message</label>
                            <textarea class="form-control" name="message"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary px-4">Send</button>
                    </form>
                </div>
            </div>


        </div>
    </section>
@endsection
