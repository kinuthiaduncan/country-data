@extends('welcome')

@section('content')
    <section class="py-5 my-2">

        <div class="container-xxl">
            <div class="row align-items-center  mb-5">


                <div class="col-lg-12">
                    <h2 class=" mb-3">Practice Areas</h2>
                    <span class="fw-bold">Economic Impact Analysis</span>
                    <p class="fw-semibold ">We help clients across all sectors to assess the contribution of activities, events, and new policy to business revenue, profits, wages, jobs, economic
                        prosperity, and public finances. We combine
                        original research with thorough data analysis to create an evidence-based story with high impact, that equip business executives with the information they need to succeed.
                    </p>
                    <span class="fw-bold">Forecasting and Scenario Building
                        </span>
                    <p class="fw-semibold ">Our set of innovative and cutting-edge forecasting techniques enable us to forecast/predict economic conditions, product demand, industrial trends, and
                        business performance on a national and regional
                        basis.
                    </p>
                    <span class="fw-bold">Business Analytics </span>
                    <p class="fw-semibold ">We support strategic business decision-making using advanced methodological techniques coupled with high quality economic and financial analysis. Our
                        priority in business analytics is to help our
                        clients gain insight and drive business planning.
                    </p>
                    <span class="fw-bold">Energy Economics</span>
                    <p class="fw-semibold ">ArcCentre advices public and private clients on a wide range of energy issues. Our areas of expertise include; regulatory advice, price controls,
                        estimation of damages, contract valuation, cost of
                        capital determination, cost-benefit analysis, feasibility studies, and policy studies.
                    </p>
                    <span class="fw-bold">Monitoring and Evaluation</span>
                    <p class="fw-semibold ">ArcCentre applies cutting-edge programs to help our clients improve performance and achieve results. We use monitoring and evaluation to assess the
                        performance of projects, institutions, and programs
                        set by our clients. Our goal in monitoring and evaluation is to improve outputs, outcomes, and impact.
                    </p>
                    <span class="fw-bold">Behavioural and Experimental Economics</span>
                    <p class="fw-semibold ">Our deep understanding of behavioural science enables us to help policy and decision makers within government, international institutions, and NGOs
                        understand the behaviour of firms and consumers.
                        Using behavioural economics, we also help businesses understand how customers make decisions. We achieve this by designing and implementing robust behavioural experiments.
                    </p>
                    <span class="fw-bold">Global Macro</span>
                    <p class="fw-semibold ">Beyond Africa, we also have a team of experts dedicated to monitoring and tracking economic events in the global economy. In particular, we focus on
                        studying major world events and policy decisions of
                        the world’s largest economies such as the US and China, since policy decisions made by those countries are capable of influencing the national economies of other countries
                        and the entire global
                        economy. The data and information we gather from our global macro experts help policy makers to formulate sensible economic policies.</p>
                    <span class="fw-bold">Climate Change and Green Finance</span>
                    <p class="fw-semibold ">Owing to the abundant evidence that climate change is already having a noticeable impact in some parts of the world, ArcCentre have incorporated climate
                        change and green finance as one of its major
                        practice areas to help tackle this problem. Our mission here is to ensure a successful implementation of climate change programs by working closely with the agencies,
                        institutions, and governments
                        that are promoting green energy and campaigning against climate change. We do not only offer the most innovative techniques to finance such programs, but also acts where we
                        add the most value to the
                        public. We aim to develop further to ensure that our work continuous to influence the decisions of regulated firms, policy makers and regulatory bodies, industry and
                        consumer groups, individual
                        consumers, and charities on green energy related matters.
                    </p>
                    <span class="fw-bold">Web Development</span>
                    <p class="fw-semibold ">We have a team of experienced web designers/developers who are adept at designing and managing professional websites from the scratch.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
