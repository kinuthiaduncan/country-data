require('./bootstrap');
import VueRouter from 'vue-router';
import store from './stores/store';

window.Vue = require('vue').default;
Vue.use(VueRouter);

Vue.component('sidebar', require('./components/SideBar.vue').default);
import Categories from './components/Categories.vue';
import Main from './components/Main.vue';
import Variable from './components/Variable';
const routes = [
    { path: '/', component: Main, name: 'main', alias: '/main' },
    { path: '/categories/:categoryId', component: Categories, name: 'categories', alias: '/categories' },
    { path: '/variable/:variableId/country/:countryId', component: Variable, name: 'variables', alias: '/variables' },
];

const router = new VueRouter({ routes });

const app = new Vue({
    router,
    store,
}).$mount('#app');
