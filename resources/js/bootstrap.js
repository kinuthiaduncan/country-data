window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
try {
    window.Popper = require('popper.js').default;
} catch (e) {}

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
