const state = {
    selectedCountry: null,
    allCountries: [],
    showCategories: false,
    categories: [],
    variables: [],
    user: null,
};

const mutations = {
    'UPDATE_SELECTED_COUNTRIES' (state, selectedCountry) {
        state.selectedCountry = selectedCountry;
    },

    'UPDATE_COUNTRIES' (state, allCountries) {
        state.allCountries = allCountries;
    },
    'UPDATE_SHOW_CATEGORIES' (state, showCategories) {
        state.showCategories = showCategories;
    },
    'UPDATE_CATEGORIES'(state, categories) {
        state.categories = categories;
    },
    'UPDATE_VARIABLES' (state, variables) {
        state.variables = variables;
    },
    'UPDATE_USER' (state, variables) {
        state.user = variables;
    },
};

const actions = {

};

const getters = {
    selectedCountry: (state) => {
        return state.selectedCountry;
    },
    allCountries: (state) => {
        return state.allCountries;
    },
    showCategories: (state) => {
        return state.showCategories;
    },
    categories: (state) => {
        return state.categories;
    },
    variables: (state) => {
        return state.variables;
    },
    user: (state) => {
        return state.user;
    },
};

export default {
    state,
    mutations,
    actions,
    getters
}
