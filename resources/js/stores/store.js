import Vue from 'vue';
import Vuex from 'vuex';

import * as actions from './actions';
import countries from './modules/countries';

Vue.use(Vuex);

export default new Vuex.Store({
    actions,
    modules: {
        countries
    }
});
