export const  updateCountryList = ({ commit }, allCountries) => {
    commit('UPDATE_COUNTRIES', allCountries);
};
export const  updateActiveCountry = ({ commit }, selectedCountry) => {
    commit('UPDATE_SELECTED_COUNTRIES', selectedCountry);
};
export const  updateShowCategories = ({ commit }, showCategories) => {
    commit('UPDATE_SHOW_CATEGORIES', showCategories);
};
export const updateCategories = ({ commit }, categoriesData) => {
    commit('UPDATE_CATEGORIES', categoriesData);
};
export const updateVariables = ({ commit }, variablesData) => {
    commit('UPDATE_VARIABLES', variablesData);
}
export const updateUser = ({ commit }, userData) => {
    commit('UPDATE_USER', userData);
}
