<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/services', function () {
    return view('services');
});
Route::get('/practice', function () {
    return view('practice');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/country-list', 'App\Http\Controllers\CountriesController@countries');
Route::get('/country/{countryName}', 'App\Http\Controllers\CountriesController@country');
Route::get('/data-center', function () {
    return view('data')->with('auth_user',  auth()->user());
});
Route::get('/api/countries', 'App\Http\Controllers\CountriesController@index');
Route::get('/api/country/{id}/data', 'App\Http\Controllers\CountriesController@getCountry');
Route::get('/api/categories/fetch', 'App\Http\Controllers\CountriesController@fetchCategories');
Route::get('/api/categories/{id}/sub-categories/fetch', 'App\Http\Controllers\CountriesController@fetchSubCategories');
Route::get('/api/variables/{variableId}/country/{countryId}/fetch', 'App\Http\Controllers\CountriesController@fetchVariableData');
Route::get('/api/variables/{variableId}/country/{countryId}/all', 'App\Http\Controllers\CountriesController@fetchAllVariableData');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
